package com.agx.kotlinmvp.http

import com.agx.agxmvp.http.HttpUtil

class ApiClient {
    companion object{
        fun getApiService(baseUrl: String = Constant.BASE_URL) = HttpUtil.createApi(ApiService::class.java,baseUrl)
    }
}
