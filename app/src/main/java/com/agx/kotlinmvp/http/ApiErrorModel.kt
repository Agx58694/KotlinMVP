package com.agx.kotlinmvp.http

data class ApiErrorModel(var status: Int, var message: String)
