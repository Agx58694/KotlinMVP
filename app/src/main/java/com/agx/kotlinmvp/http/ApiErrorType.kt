package com.agx.kotlinmvp.http

import android.content.Context
import android.support.annotation.StringRes
import com.agx.agxmvp.R

enum class ApiErrorType(val code: Int, @param: StringRes private val messageId: Int) {
    INTERNAL_SERVER_ERROR(500, R.string.service_error),
    BAD_GATEWAY(502, R.string.service_error),
    NOT_FOUND(404, R.string.not_found),
    CONNECTION_TIMEOUT(408, R.string.timeout),
    NETWORK_NOT_CONNECT(499, R.string.network_wrong),
    UNEXPECTED_ERROR(700, R.string.unexpected_error),
    NOT_LOGIN(401,R.string.not_login),
    SERVICE_FORBIDDEN(403,R.string.service_forbidden),
    JSON_ERROR(1001,R.string.json_error);

    fun getApiErrorModel(context: Context): ApiErrorModel {
        return ApiErrorModel(code, context.getString(messageId))
    }
}
