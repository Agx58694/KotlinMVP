package com.agx.kotlinmvp.http

import com.agx.kotlinmvp.model.ResponseWrapper
import com.agx.kotlinmvp.model.HttpModel
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*


interface ApiService {

    @Multipart
    @POST("xxxx/xxxx") //This is imaginary URL
    fun updateImage(@Part("name") name: RequestBody,
                    @Part image: MultipartBody.Part): Observable<String>

    //获取验证码
    @FormUrlEncoded
    @POST("vcode")
    fun getVCode(@Field("code_type") code_type: String,
                 @Field("phone") phone: String,
                 @Field("pre") pre: String): Observable<ResponseWrapper<HttpModel>>

}
