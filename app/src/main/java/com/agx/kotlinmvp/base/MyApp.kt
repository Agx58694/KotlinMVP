package com.agx.kotlinmvp.base

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.agx.agxmvp.http.HttpUtil
import com.agx.kotlinmvp.http.ApiClient
import com.agx.kotlinmvp.http.Constant

/**
 *
 * @author Agx
 * @date 2018/11/21
 */

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        //初始化OkHttp
        HttpUtil.initOkHttpClient()
        init()
    }

    private fun init() {}

    companion object {

        @SuppressLint("StaticFieldLeak")
        var context: Context? = null
            private set
    }
}
