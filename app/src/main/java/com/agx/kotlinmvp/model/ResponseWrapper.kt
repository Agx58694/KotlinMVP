package com.agx.kotlinmvp.model

data class ResponseWrapper<T>(var code: Int, var data: T, var desc: String)
