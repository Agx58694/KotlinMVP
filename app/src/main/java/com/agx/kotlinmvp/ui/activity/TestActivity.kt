package com.agx.kotlinmvp.ui.activity

import com.agx.kotlinmvp.R
import com.agx.agxmvp.base.BaseActivity
import com.agx.kotlinmvp.ui.presenter.TestPresenter
import com.agx.kotlinmvp.ui.view.TestView

class TestActivity : BaseActivity<TestView, TestPresenter>(),TestView {
    override fun showLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun closeLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPhoneCodeSuccess() {

    }

    override fun contentViewId(): Int {
        return R.layout.activity_main

    }

    override fun createPresenter(): TestPresenter {
        return TestPresenter(this,lifecycleSubject)
    }

    override fun initOperation() {
        mPresenter.getPhoneCode("101","15519566348","86")
    }

    override fun loadError(s: String?) {

    }
}