package com.agx.kotlinmvp.ui.presenter

import android.app.Activity
import com.agx.agxmvp.base.BasePresenter
import com.agx.kotlinmvp.http.ApiErrorModel
import com.agx.kotlinmvp.http.NetworkScheduler
import com.agx.kotlinmvp.http.RequestCallback
import com.agx.kotlinmvp.http.ApiClient
import com.agx.kotlinmvp.model.HttpModel
import com.agx.kotlinmvp.ui.view.TestView
import com.trello.rxlifecycle2.android.ActivityEvent
import com.trello.rxlifecycle2.kotlin.bindUntilEvent
import io.reactivex.subjects.BehaviorSubject

class TestPresenter(context: Activity, lifecycleSubject: BehaviorSubject<ActivityEvent>) : BasePresenter<TestView>(context,lifecycleSubject){

    fun getPhoneCode(codeType: String,phone: String,pre: String){
        ApiClient.getApiService().getVCode(codeType,phone,pre)
                .compose(NetworkScheduler.compose())
                .bindUntilEvent(mLifecycleSubject,ActivityEvent.DESTROY)
                .subscribe(object : RequestCallback<HttpModel>(mContext){
                    override fun success(data: HttpModel) {
                        view?.getPhoneCodeSuccess()
                    }

                    override fun failure(statusCode: Int, apiErrorModel: ApiErrorModel) {
                        view?.loadError(apiErrorModel.message)
                    }
                })
    }
}