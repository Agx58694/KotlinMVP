package com.agx.kotlinmvp.ui.view

import com.agx.agxmvp.base.BaseView

interface TestView : BaseView {
    fun getPhoneCodeSuccess()
}