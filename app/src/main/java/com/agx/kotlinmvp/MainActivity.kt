package com.agx.kotlinmvp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.agx.kotlinmvp.ui.activity.TestActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startActivity(Intent(this,TestActivity::class.java))
    }
}
