package com.agx.agxmvp.base

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter

abstract class MyBaseAdapter<T>(var datas: List<T>, var context: Context) : BaseAdapter() {

    override fun getCount(): Int {
        return datas.size
    }

    override fun getItem(position: Int): T {
        return datas[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    abstract override fun getView(position: Int, view: View, arg2: ViewGroup): View


}
