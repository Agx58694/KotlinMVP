package com.agx.agxmvp.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import butterknife.Unbinder
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog

/**
 * Created by Agx on 2018/11/21.
 */

abstract class BaseFragment<V, T : BasePresenter<V>> : Fragment() {

    private lateinit var mBind: Unbinder
    protected lateinit var mPresenter: T
    private lateinit var tipDialog: QMUITipDialog


    //得到当前界面的布局文件id(由子类实现)
    protected abstract val contentViewId: Int

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()

        //判断是否使用MVP模式
        mPresenter = createPresenter()
        mPresenter.attachView(this as V)//因为之后所有的子类都要实现对应的View接口
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //子类不再需要设置布局ID，也不再需要使用ButterKnife.bind()
        val rootView = inflater.inflate(contentViewId, container, false)
        mBind = ButterKnife.bind(this,rootView)
        try {
        } catch (e: Exception) {
            Log.d("this", e.message)
        }

        initView(rootView)
        return rootView
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initOperation()
        initListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        mBind.unbind()
        mPresenter.detachView()
    }

    open fun init() {}

    open fun initView(rootView: View) {}
    //用于创建Presenter和判断是否使用MVP模式(由子类实现)
    protected abstract fun createPresenter(): T

    protected abstract fun initOperation()

    open fun initListener() {}

    fun showDialog() {
        try {
            closeDialog()
            tipDialog = QMUITipDialog.Builder(context)
                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
                    .setTipWord("正在加载")
                    .create()
            tipDialog.show()
        } catch (ignored: Exception) {
        }

    }

    fun closeDialog() {
        try {
            tipDialog.dismiss()
        } catch (ignored: Exception) {
        }

    }

}
