package com.agx.agxmvp.base

import android.app.Activity
import com.trello.rxlifecycle2.android.ActivityEvent
import io.reactivex.subjects.BehaviorSubject

import java.lang.ref.Reference
import java.lang.ref.WeakReference


open class BasePresenter<V>(context: Activity,lifecycleSubject: BehaviorSubject<ActivityEvent>) {

    protected var mContext: Activity = context
    protected var mLifecycleSubject = lifecycleSubject

    private var mViewRef: Reference<V>? = null

    val isViewAttached: Boolean
        get() = mViewRef != null && mViewRef!!.get() != null

    val view: V?
        get() = if (mViewRef != null) mViewRef!!.get() else null

    internal fun attachView(view: V) {
        mViewRef = WeakReference(view)
    }

    internal fun detachView() {
        if (mViewRef != null) {
            mViewRef!!.clear()
            mViewRef = null
        }
    }

}