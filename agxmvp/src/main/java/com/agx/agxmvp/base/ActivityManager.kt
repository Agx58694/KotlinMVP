package com.agx.agxmvp.base

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.ImageView
import android.widget.ListView

import java.util.Stack

class ActivityManager
/**
 * constructor
 */
private constructor() {

    /**
     * add Activity to Stack
     */
    fun addActivity(activity: Activity) {
        if (activityStack == null) {
            activityStack = Stack()
        }
        activityStack?.add(activity)
    }


    /**
     * remove Activity from Stack
     */
    fun removeActivity(activity: Activity) {
        if (activityStack == null) {
            activityStack = Stack()
        }
        activityStack!!.remove(activity)
    }

    /**
     * get current activity from Stack
     */
    fun currentActivity(): Activity {
        return activityStack!!.lastElement()
    }

    fun finishActivity() {
        val activity = activityStack!!.lastElement()
        finishActivity(activity)
    }

    fun finishActivity(activity: Activity?) {
        if (activity != null) {
            activityStack!!.remove(activity)
            activity.finish()
        }
    }
    //
    //	public MainActivity getMainActivity(){
    //		MainActivity main = null;
    //		for (Activity a:activityStack) {
    //			if (a instanceof MainActivity)main= (MainActivity) a;
    //		}
    //		return main;
    //	}

    fun finishActivity(cls: Class<*>) {
        for (activity in activityStack!!) {
            if (activity.javaClass == cls) {
                finishActivity(activity)
            }
        }
    }

    fun finishAllActivity() {
        for (activity in activityStack!!) {
            activity?.finish()
        }
        activityStack?.clear()
        //杀死该应用进程
        //		android.os.Process.killProcess(android.os.Process.myPid());

    }

    /**
     * exit System
     * @param context
     */
    fun exit(context: Context) {
        exit()
    }

    /**
     * exit System
     */
    fun exit() {
        try {
            finishAllActivity()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    companion object {

        var activityStack: Stack<Activity>? = null
        private var instance: ActivityManager? = null

        /**
         * get the AppManager instance, the AppManager is singleton.
         */
        fun getInstance(): ActivityManager {
            if (instance == null) {
                instance = ActivityManager()
            }
            return instance as ActivityManager
        }

        fun unbindReferences(view: View?) {
            try {
                if (view != null) {
                    view.destroyDrawingCache()
                    unbindViewReferences(view)
                    if (view is ViewGroup) {
                        unbindViewGroupReferences((view as ViewGroup?)!!)
                    }
                }
            } catch (e: Throwable) {

            }

        }

        private fun unbindViewGroupReferences(viewGroup: ViewGroup) {
            val nrOfChildren = viewGroup.childCount
            for (i in 0 until nrOfChildren) {
                val view = viewGroup.getChildAt(i)
                unbindViewReferences(view)
                if (view is ViewGroup) {
                    unbindViewGroupReferences(view)
                }
            }
            try {
                viewGroup.removeAllViews()
            } catch (mayHappen: Throwable) {
                // AdapterViews, ListViews and potentially other ViewGroups don't
                // support the removeAllViews operation
            }

        }

        private fun unbindViewReferences(view: View) {
            // set all listeners to null (not every view and not every API level
            // supports the methods)
            try {
                view.setOnClickListener(null)
                view.setOnCreateContextMenuListener(null)
                view.onFocusChangeListener = null
                view.setOnKeyListener(null)
                view.setOnLongClickListener(null)
                view.setOnClickListener(null)
            } catch (mayHappen: Throwable) {

            }

            // set background to null
            var d: Drawable? = view.background
            if (d != null) {
                d.callback = null
            }

            if (view is ImageView) {
                d = view.drawable
                if (d != null) {
                    d.callback = null
                }
                view.setImageDrawable(null)
                view.setBackgroundDrawable(null)
            }

            // destroy WebView
            if (view is WebView) {
                var webview: WebView? = view
                webview!!.stopLoading()
                webview.clearFormData()
                webview.clearDisappearingChildren()
                webview.webChromeClient = null
                webview.webViewClient = null
                webview.destroyDrawingCache()
                webview.destroy()
                webview = null
            }

            if (view is ListView) {
                try {
                    view.removeAllViewsInLayout()
                } catch (mayHappen: Throwable) {
                }

                view.destroyDrawingCache()
            }
        }
    }
}
