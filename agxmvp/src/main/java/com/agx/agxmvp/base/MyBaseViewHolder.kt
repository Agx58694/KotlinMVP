package com.agx.agxmvp.base

import android.view.View

import com.chad.library.adapter.base.BaseViewHolder

/**
 *
 * @author Agx
 * @date 2017/5/3
 */

class MyBaseViewHolder(view: View) : BaseViewHolder(view) {
    //这里使用了Fresco 加载框架，请注意一定要传SimpleDraweeView类型的view
    //渐进式图片加载
//    fun setSimpleDraweeViewUrl(viewId: Int, url: String, context: Context): MyBaseViewHolder {
//        val view = this.getView<SimpleDraweeView>(viewId)
//        val request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(url))
//                .setProgressiveRenderingEnabled(true)
//                .build()
//        val controller = Fresco.newDraweeControllerBuilder()
//                .setImageRequest(request)
//                .setOldController(view.controller)
//                .build()
//        view.controller = controller
//        return this
//    }

}
