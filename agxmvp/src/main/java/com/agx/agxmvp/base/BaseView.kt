package com.agx.agxmvp.base

/**
 * Created by guoyuhang on 2016/11/23.
 */

interface BaseView {
    fun loadError(s: String?)

    fun showLoading()

    fun closeLoading()
}
