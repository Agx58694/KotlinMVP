package com.agx.agxmvp.base

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import butterknife.ButterKnife
import butterknife.Unbinder
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog
import com.trello.rxlifecycle2.LifecycleProvider
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.RxLifecycle
import com.trello.rxlifecycle2.android.ActivityEvent
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


/**
 * @author guoyuhang
 */
@Suppress("UNCHECKED_CAST")
abstract class BaseActivity<V, T : BasePresenter<V>> : AppCompatActivity(), LifecycleProvider<ActivityEvent>{

    private lateinit var tipDialog: QMUITipDialog
    private lateinit var mBind: Unbinder
    protected lateinit var mPresenter: T
    val lifecycleSubject: BehaviorSubject<ActivityEvent> = BehaviorSubject.create()

    /**
     * 得到当前界面的布局文件id(由子类实现) */
    protected abstract fun contentViewId(): Int


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT// 锁定竖屏

        ActivityManager.getInstance().addActivity(this)
        //判断是否使用MVP模式
        mPresenter = createPresenter()
        mPresenter.attachView(this as V)//因为之后所有的子类都要实现对应的View接口
        init()
        try {
            setContentView(contentViewId())
            mBind = ButterKnife.bind(this)
        } catch (e: Exception) {
            Log.d("setContentView", e.message)
        }

        initOperation()
        initListener()
    }

    fun showDialog() {
        try {
            closeDialog()
            tipDialog = QMUITipDialog.Builder(this)
                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
                    .setTipWord("正在加载")
                    .create()
            tipDialog.show()
        } catch (ignored: Exception) {
        }

    }

    fun closeDialog() {
        try {
            tipDialog.dismiss()
        } catch (ignored: Exception) {
        }

    }

    override fun lifecycle(): Observable<ActivityEvent> {
        return lifecycleSubject.hide()
    }

    override fun <T : Any?> bindUntilEvent(event: ActivityEvent): LifecycleTransformer<T> {
        return RxLifecycle.bindUntilEvent(lifecycleSubject,event)
    }

    override fun <T : Any?> bindToLifecycle(): LifecycleTransformer<T> {
        return RxLifecycleAndroid.bindActivity(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()
        mBind.unbind()
        lifecycleSubject.onNext(ActivityEvent.DESTROY)
        mPresenter.detachView()
        ActivityManager.getInstance().removeActivity(this)
    }

    override fun onStop() {
        super.onStop()
        lifecycleSubject.onNext(ActivityEvent.STOP)
    }

    override fun onPause() {
        super.onPause()
        lifecycleSubject.onNext(ActivityEvent.PAUSE)
    }

    override fun onResume() {
        super.onResume()
        lifecycleSubject.onNext(ActivityEvent.RESUME)
    }

    override fun onStart() {
        super.onStart()
        lifecycleSubject.onNext(ActivityEvent.START)
    }

    /**
     * 用于创建Presenter和判断是否使用MVP模式(由子类实现)
     */
    protected abstract fun createPresenter(): T

    protected abstract fun initOperation()

    open fun init() {}

    open fun initView(){}

    open fun initListener() {}

}
